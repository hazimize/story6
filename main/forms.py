from django import forms
from .models import Acara, PesertaAcara

class AcaraForm(forms.ModelForm):
    class Meta:
        model = Acara
        fields=[
            'name',
            'deskripsi'
        ]
class PesertaAcaraForm(forms.ModelForm):
    class Meta:
        model = PesertaAcara
        fields=[
            'name'
        ]
