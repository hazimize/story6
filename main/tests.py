from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from .models import Acara,PesertaAcara
from .forms import AcaraForm, PesertaAcaraForm


# @tag('functional')
# class FunctionalTestCase(LiveServerTestCase):
#     """Base class for functional test cases with selenium."""

#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         # Change to another webdriver if desired (and update CI accordingly).
#         options = webdriver.chrome.options.Options()
#         # These options are needed for CI with Chromium.
#         options.headless = True  # Disable GUI.
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         cls.selenium = webdriver.Chrome(options=options)

#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()


# class MainTestCase(TestCase):
#     def test_root_url_status_200(self):
#         response = self.client.get('/')
#         self.assertEqual(response.status_code, 200)
#         # You can also use path names instead of explicit paths.
#         response = self.client.get(reverse('main:home'))
#         self.assertEqual(response.status_code, 200)


# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())

class TestKegiatan(TestCase):
    def test_kegiatan_url_is_exist(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code,200)
    def test_template_kegiatan(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'main/kegiatan.html')
    def test_kelengkapan_halaman_kegiatan(self):
        response=Client().get('/kegiatan/')
        isi_html=response.content.decode('utf8')
        self.assertIn("KEGIATANKU", isi_html)
        # self.assertIn("Masukkan Kegiatan", isi_html)
        # self.assertIn("Tambah peserta", isi_html)
    # def test_url_hasil(self)
    def test_models_acara(self):
        Acara.objects.create(name="contoh", deskripsi="cont")
        jumlah_data = Acara.objects.all().count()
        self.assertEquals(jumlah_data,1)
    def test_models_peserta(self):
        PesertaAcara.objects.create(name="contoh")
        jumlah_data = PesertaAcara.objects.all().count()
        self.assertEquals(jumlah_data,1)
    def test_forms_acara(self):
        data = {'name': 'something', 'deskripsi':'duh bingung nih'}
        acaraform=AcaraForm(data)
        self.assertTrue(acaraform.is_valid())
        self.assertEqual(acaraform.cleaned_data['name'], 'something')
        self.assertEqual(acaraform.cleaned_data['deskripsi'],'duh bingung nih')
    def test_forms_peserta(self):
        data = {'name': 'something'}
        pesertaacaraform=PesertaAcaraForm(data)
        self.assertTrue(pesertaacaraform.is_valid())
        self.assertEqual(pesertaacaraform.cleaned_data['name'], 'something')
