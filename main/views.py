from django.shortcuts import render, redirect
from .forms import AcaraForm, PesertaAcaraForm
from .models import Acara, PesertaAcara
def home(request):
    return render(request, 'main/home.html')
def kegiatan(request):
    if request.method=="POST":
        form=AcaraForm(request.POST)
        if form.is_valid():
            acr= Acara()
            acr.name= form.cleaned_data['name']
            acr.deskripsi= form.cleaned_data['deskripsi']
            acr.save()
        return redirect('/kegiatan')
    else:
            acr = Acara.objects.all()
            peserta= PesertaAcara.objects.all()
            form = AcaraForm()
            formpes= PesertaAcaraForm()
            response = {"acr":acr, 'form':form, 'peserta':peserta, 'formpes':formpes}
            return render (request, 'main/kegiatan.html',response)

def daftarAcara(request):
    if request.method=="POST":
        form=PesertaAcaraForm(request.POST)
        if form.is_valid():
            peserta= PesertaAcara()
            peserta.name= form.cleaned_data['name']
            peserta.save()
        return redirect('/daftarAcara')
    else:
        peserta = PesertaAcara.objects.all()
        formpeserta = PesertaAcaraForm()
        response = {"peserta":peserta, 'form':formpeserta}
        return render (request, 'main/peserta.html',response)
